# Club Updates

Club updates go here

# Cybersecurity news week of yyyy-mm-dd

## Story 1 title

Summary goes here

- Links: 
- Severity: (0 min, 10 max)
## Story 2 title

Summary goes here

- Links:
- Severity: (0 min, 10 max)
## Story 3 title

Summary goes here

- Links: 
- Severity: (0 min, 10 max)


Join our Riot to discuss cybersecurity news: https://matrix.to/#/!FcedFCDPjwhAgQUgRM:matrix.org?via=matrix.org
You can also contribute to these emails directly on GitLab: https://gitlab.com/ColoradoSchoolOfMines/oresec-docs
