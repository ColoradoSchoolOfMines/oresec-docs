# Talk Ideas

This document is a place to keep future meeting ideas orginized

- Web exploitation workshop - Fisher
- GPG key setup - Jake
- HackTheBox - Ivan
  - VPNs
- Introduction to Pen-testing - ???
- Linux system hardning (servers) - ???
- Windows system hardning(servers) - ???
- Ethics - ??? (could be done in intro talk)
  - Removing liability from the club
- CTF problems
  - Binary exploitation
  - Reverse engineering
  - Forensics
  - etc.

## Introduction to Security

- Topics
  - CTFs
  - BugBounties
  - Resarch
  - Open Source projects
  - Red Team / Blue Team
- Ethics
  - Whitehat vs Blackhat
  - Responsible Disclosure (bug bounties)
  - Legality
  - CYA
  - Mines specific systems
