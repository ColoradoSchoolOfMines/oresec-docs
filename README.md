# oresec-docs

This is the repo for managing the details of administration of the
Mines Cybersecurity club. Also is a place to orginize the weekly
cybersecurity update emails before being sent.

You might be looking for our [website](https://oresec.mines.edu/). 

## Contributing

Check out the
[issues](https://gitlab.com/ColoradoSchoolOfMines/oresec-docs/issues)
page to contribute to the work in progress updates, feel free to post
articles, ask questions, etc. The text for the update will be in the
`weekly-updates` folder, pull requests for stories are welcome!
